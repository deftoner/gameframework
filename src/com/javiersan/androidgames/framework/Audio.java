package com.javiersan.androidgames.framework;

public interface Audio {
	public Music newMusic(String filename);

	public Sound newSound(String filename);
}
